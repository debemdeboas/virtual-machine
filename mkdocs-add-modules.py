import jinja2
import os
import sys

if len(sys.argv) == 2:
    module_path = sys.argv[2]
else:
    module_path = 'src'

modules = set()
for _, dir_list, files in os.walk(top=module_path, topdown=True):
    if '__init__.py' in set(files):
        for dir_ in dir_list:
            if dir_ not in modules:
                modules.add(dir_)

if '__pycache__' in modules:
    modules.remove('__pycache__')

doc_template = jinja2.Environment(loader=jinja2.FileSystemLoader('.')).get_template('documentation.md.j2')

for module in modules:
    with open(f'docs/modules/{module}.md', 'w') as f:
        f.write(doc_template.render(module=module))
